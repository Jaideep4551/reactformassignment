import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import { makeStyles, CssBaseline, createMuiTheme, ThemeProvider } from '@material-ui/core';
import Users from "../pages/User/UserGrid";
import Createuser from "../pages/User/createuser";


const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#333996",
      light: '#3c44b126'
    },
    secondary: {
      main: "#f83245",
      light: '#f8324526'
    },
    background: {
      default: "#f4f5fd"
    },
  },
  overrides:{
    MuiAppBar:{
      root:{
        transform:'translateZ(0)'
      }
    }
  },
  props:{
    MuiIconButton:{
      disableRipple:true
    }
  }
})


const useStyles = makeStyles({
   
  })

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <Router>
      <div className={classes.appMain}>
        
      <Createuser />
        <div className ="navigation_bar">
        <ul key = "nav">
        <li>
        <Link className = "link" to="/users/create">Create</Link>

            </li>
                    <li><Link className = "link" to="/users/view">View</Link></li>
                </ul></div>
                <Switch>
                <Route exact path = "/">
                <Redirect to="/users/create" />
                </Route>
                <Route path = "/users/view">
                  <View />
                </Route>
        
                
          
          
        </Switch>
      </div>
      </Router>
      <CssBaseline />
    </ThemeProvider>
  );
}
function Home(){
  return <Createuser />
}

function View(){
  return <Users />;
}


export default App;
