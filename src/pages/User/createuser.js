import React, { useState } from 'react'
import UserForm from "./UserForm";
import AddIcon from '@material-ui/icons/Add';
import Controls from "../../components/controls/Controls";
import { makeStyles } from "@material-ui/core";
import Popup from "../../components/Popup";
import * as userService from "../../services/UserService";


const useStyles = makeStyles(theme => ({
    pageContent: {
        margin: theme.spacing(5),
        padding: theme.spacing(3)
    },
    searchInput: {
        width: '75%'
    },
    newButton: {
        position: 'absolute',
        right: '10px',

    }
}))


export default function User() {

    const classes = useStyles();
    const [recordForEdit, setRecordForEdit] = useState(null)
    const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })
    const [records, setRecords] = useState(userService.getAllUser())
    const [openPopup, setOpenPopup] = useState(false)
    const [notify, setNotify] = useState({ isOpen: false, message: '', type: '' })
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })

    const openInPopup = item => {
        setRecordForEdit(item)
        setOpenPopup(true)
    }

    const addOrEdit = (user, resetForm) => {
        if (user.id == 0)
            userService.insertUser(user)
            resetForm()
        setRecordForEdit(null)
        setOpenPopup(false)
        setRecords(userService.getAllUser())
    }
    return (
        <div>

                    <Controls.Button
                        text="Create New User"
                        variant="outlined"
                        startIcon={<AddIcon />}
                        className={classes.newButton}
                        onClick={() => { setOpenPopup(true); setRecordForEdit(null); }}
                    />


                <Popup
                title="user Form"
                openPopup={openPopup}
                setOpenPopup={setOpenPopup}
            >
                <UserForm
                    recordForEdit={recordForEdit}
                    addOrEdit={addOrEdit} />
            </Popup>
        </div>


    )
}